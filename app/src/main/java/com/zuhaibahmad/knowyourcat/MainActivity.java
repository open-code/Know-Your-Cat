package com.zuhaibahmad.knowyourcat;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = "KnowYourCat";
    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private LinearLayout container;
    private TextView factTextView;
    private ProgressBar progressbar;
    private ImageView submitButton, saveFactButton, copyFactButton;

    //============================================================================================//
    //                                  Reference Type Variables                                  //
    //============================================================================================//
    private RequestQueue queue;
    private AdView topAdView, bottomAdView;
    private FactsDatabase database;

    //============================================================================================//
    //                                  Reference Type Variables                                  //
    //============================================================================================//
    private String fact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupLayout();
        database = new FactsDatabase(this);
        getFact();
    }

    private void setupLayout() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.MISave);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SavesActivity.class));
            }
        });

        container = (LinearLayout) findViewById(R.id.container);

        progressbar = (ProgressBar) findViewById(R.id.progressBar);
        factTextView = (TextView) findViewById(R.id.TVFact);
        submitButton = (ImageView) findViewById(R.id.BTSubmit);
        copyFactButton = (ImageView) findViewById(R.id.BTCopyFact);
        saveFactButton = (ImageView) findViewById(R.id.BTSaveFact);

        submitButton.setOnClickListener(this);
        copyFactButton.setOnClickListener(this);
        saveFactButton.setOnClickListener(this);

        topAdView = (AdView) findViewById(R.id.AVTop);
        bottomAdView = (AdView) findViewById(R.id.AVBottom);
        AdRequest adRequest = new AdRequest.Builder().build();
        topAdView.loadAd(adRequest);
        bottomAdView.loadAd(adRequest);
    }

    private void getFact() {
        String URL = "http://catfacts-api.appspot.com/api/facts";

        showProgressbar();

        if (Utils.isNetworkOnline(this)) {
            StringRequest request = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    fact = parseFact(s);
                    factTextView.setText(fact);
                    hideProgressbar();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    hideProgressbar();
                    Utils.showAlertDialogWithoutCancel(MainActivity.this, "Error", volleyError.getMessage());
                }
            });
            if (queue == null) queue = Volley.newRequestQueue(this);
            queue.add(request);
        } else {
            hideProgressbar();
            Utils.showAlertDialogWithoutCancel(this, "Connection Error", "Network Not Found! Please Connect To A Network And Try Again");
        }
    }

    private void showProgressbar() {
        submitButton.setVisibility(View.GONE);
        progressbar.setVisibility(View.VISIBLE);
    }

    private void hideProgressbar() {
        submitButton.setVisibility(View.VISIBLE);
        progressbar.setVisibility(View.GONE);
    }

    private String parseFact(String s) {
        String fact = null;
        try {
            JSONObject object = new JSONObject(s);
            JSONArray array = object.getJSONArray("facts");
            fact = array.getString(0);
            Log.e(TAG,"Fact: " + fact);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return fact;
    }

    private void saveFact() {
        boolean success = false;
        try {
            database.open();
            success = database.addNewItem(fact);
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Utils.showAlertDialogWithoutCancel(this, "Error", "Error Occurred While Accessing Database");
        }

        if(success)
            Utils.showLightSnackbar(container, "Fact Saved!");
    }

    private void copyFact() {
        if(Utils.copyToClipboard(this,fact))
            Utils.showLightSnackbar(container,"Copied To Clipboard!");
    }

    /**
     * Called when leaving the activity
     */
    @Override
    public void onPause() {
        if (topAdView != null) {
            topAdView.pause();
        }
        if (bottomAdView != null) {
            bottomAdView.pause();
        }
        super.onPause();
    }

    /**
     * Called when returning to the activity
     */
    @Override
    public void onResume() {
        super.onResume();
        if (topAdView != null) {
            topAdView.resume();
        }
        if (bottomAdView != null) {
            bottomAdView.resume();
        }
    }

    /**
     * Called before the activity is destroyed
     */
    @Override
    public void onDestroy() {
        if (topAdView != null) {
            topAdView.destroy();
        }
        if (bottomAdView != null) {
            bottomAdView.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if(id == R.id.BTSubmit){
            getFact();
        }else if(id == R.id.BTCopyFact){
            copyFact();
        }else if(id == R.id.BTSaveFact){
            saveFact();
        }
    }
}

Know Your Cat is a simple application that provides you interesting facts about cats, so you can know more about them!  

Features: 
- Shuffle Facts 
- Copy to clipboard for sharing 
- Save as favorite 
- Favorite facts